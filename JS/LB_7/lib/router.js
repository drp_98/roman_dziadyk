var ctrl = require('../controllers');

exports.router = function (req, res) {
    switch (req.url) {
        case '/':
            ctrl.indexController(req, res);
            break;
        case '/style.css':
            ctrl.css(req,res);
            break;
        case '/script.js':
            ctrl.script(req,res);
           break;
        case '/jquery-3.2.1.min.js':
            ctrl.jquery(req,res);
            break;
        case '/db/db.json':
            ctrl.database(req, res);
            break;
        default:
            ctrl.notFound(req, res);
    }
}