function Person (name, surname, patronymic) {
	this.name = name;
	this.surname = surname;
	this.patronymic = patronymic;
}

function Boxer (name, surname, patronymic, weight_category) {
	Person.call(this, name, surname, patronymic);
	this.weight_category = weight_category;
}

Boxer.prototype = Person.prototype;
Boxer.prototype.constructor = Boxer;

$(document).ready(function(){

	var isNameValid = false, isSurnameValid = false, isPatronymicValid = false, isWeight_categoryValid = false;

	submitControl();
	function submitControl(){
		if(isNameValid && isSurnameValid && isPatronymicValid && isWeight_categoryValid)
			$('#submit').prop("disabled", false);
		else
			$('#submit').prop("disabled", true);
	}

	$('#name').on('input keyup', function() {
		if($('#name').val() === '') {
			isSurnameValid = false;
			$('#name').removeClass('success').addClass('error');
		}
		else {
			isSurnameValid = true;
			$('#name').removeClass('error').addClass('success');
		}
		submitControl();
	});


	$('#surname').on('input keyup', function() {
		if($('#surname').val() === '') {
			isPatronymicValid = false;
			$('#surname').removeClass('success').addClass('error');
		}
		else {
			isPatronymicValid = true;
			$('#surname').removeClass('error').addClass('success');
		}
		submitControl();
	});

	$('#patronymic').on('input keyup', function() {
		if($('#patronymic').val() === '') {
			isWeight_categoryValid = false;
			$('#patronymic').removeClass('success').addClass('error');
		}
		else {
			isWeight_categoryValid = true;
			$('#patronymic').removeClass('error').addClass('success');
		}
		submitControl();
	});

	$('#weight_category').on('input keyup', function() {
		if($('#weight_category').val() === '') {
			isNameValid = false;
		}
		else {
			isNameValid = true;
		}
		submitControl();
	});

	$('#submit').click(function(e){
		e.preventDefault();
		var newBoxer = new Boxer(
			$('#name').val(),
			$('#surname').val(),
			$('#patronymic').val(),
			$('#weight_category').val(),
		);
		$('<div class="container">' + '<li>' + newBoxer.name + " " + newBoxer.surname + " " + newBoxer.patronymic + " " + newBoxer.weight_category + '</li>' + '<button id="btn">x</button>' + '<div class="clear"></div>' + '</div>').hide().appendTo('#boxers_list').fadeIn(1000);
		if ($('#weight_category :selected').val()==="Heavyweight") {
			$('li:last').addClass('color');
		}
/*
		var database = require('fs');
		exports.database = function(req, res){
			database.writeFile('./db/db.json', JSON.stringify(Boxer), function(err) {
				if(err) {
					return console.log(err);
				}
				console.log("The file was saved!");
			});
		}*/
	

		
		//                 var string = JSON.stringify(newBoxer);
		//newBoxer = JSON.parse(leaderStr);
		//console.log(string);
		//                 module.exports = string;



	});

	$(document).on("click", "#btn", function(e) {
		e.preventDefault();
		$(this).parent().remove();
	});
});
