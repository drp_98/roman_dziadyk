function printArray (array) {
	console.log("array : "+array);
}

function fillArrayRandom (array) {
	for (var i = 0; i < array.length; i++) {
		array[i] = Math.round(Math.random()*20);
	}
}

function searchIndex(number, arr) {
	console.log("index of number "+number+" :");
	var result = -1;
	for(var i = 0; i < arr.length; i++) {
		if (number === arr[i]){
			result = i;	        
			console.log(result);
		}	
	}
	if (result < 0) {
		console.log("element does not exist in current array!");
	}
}

function searchIndexInUserArray (number,arr) {
	console.log("---SearchIndexInUserArray---");
	searchIndex(number,arr);
	printArray(arr);
}

function searcIndexInRandomArray (number,length_array) {
	console.log("---SearcIndexInRandomArray---");
	var arr = new Array(length_array);
	fillArrayRandom(arr);
	searchIndex(number,arr);
	printArray(arr);
}

searchIndexInUserArray (3, [1,1,2,2,5,3,2,3,2,5,10,3]);
searcIndexInRandomArray (5, 10);