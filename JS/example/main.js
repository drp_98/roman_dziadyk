function Student(name, age){
	this.name = name;
	this.age = age;
}


window.onload = function() {
	var nameField = document.getElementById('name');
	var ageField = document.getElementById('age');
	var submitButton = document.getElementById('submit');
	var isNameValid = false, isAgeValid = false;
	var studentList = document.getElementById('students-list');
	var list = [];

	submitControl();
	function submitControl(){
		if(isNameValid && isAgeValid)
			submitButton.disabled = false;
		else
			submitButton.disabled = true;
	}

	nameField.oninput = function(){
		if(nameField.value === ''){
			isNameValid = false;
			nameField.className = 'error';
		}
		else{
			isNameValid = true;
			nameField.className = 'success';
		}
		submitControl();
	}

	ageField.oninput = function(){
		if(ageField.value === ''){
			isAgeValid = false;
			ageField.className = 'error';
		}
		else{
			isAgeValid = true;
			ageField.className = 'success';
		}
		submitControl();
	}

	submitButton.onclick = function(event){
		event.preventDefault();	
		list.push(new Student(nameField.value, ageField.value));
		var elem = document.createElement('li');
		elem.innerHTML = list[list.length - 1].name;
		studentList.appendChild(elem);

	}
}