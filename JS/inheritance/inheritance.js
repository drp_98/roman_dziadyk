/*console.log("---------------");

var string = "yol";
var str = new String("kek");

console.log(string);
console.log(str);

str.gfh = "111";

console.log(string[0]);*/




/*
class Cat {
	constructor (length, age) {
        this.length = length;
        this.age = age;
    }

    eat () {

    }
}

var cat1 = new Cat(0.5, 5);
console.log(cat1);

var cat2 = new Cat(1, 10);
console.log(cat2);*/

/*
function Cat (length, age) {
    this.length = length;
    this.age = age;

    this.eat = function() {

    }
}

var cat1 = new Cat(0.5, 5);
console.log(cat1);

var cat2 = new Cat(1, 10);
console.log(cat2);*/

/*
var animal = {
    age: 10,
    eat: function() {

    }
}

var cat = {
    mustache: true,
    getVoice: function() {
        return 'myayya'
    }
};

var dog = {
    mustache: false,
    getVoice: function() {
        return 'gafgaf'
    }
};

cat.__proto__ = animal;
dog.__proto__ = animal;

console.log(cat.getVoice());
console.log(cat.age);*/

class Animal {
    constructor(age) {
        this.age = age;
    }
    eat() {

    }
}

class Cat extends Animal {
    constructor(mustache) {
        super(4);
        this.mustache = mustache;
    }
    getVoice() {
        return 'myayya'
    }
};

var cat = new Cat(3);

console.log(cat);

//function Animal() {
//    this.age = 10;
//}
//
//function Cat() {
//    this.tail = 10;
//}
//
//Cat.prototype = new Animal();