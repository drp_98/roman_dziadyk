function Person (name, surname, patronymic) {
	this.name = name;
	this.surname = surname;
	this.patronymic = patronymic;
}

function Boxer (name, surname, patronymic, weight_category) {
	Person.call(this, name, surname, patronymic);
	this.weight_category = weight_category;
}

Boxer.prototype = Person.prototype;
Boxer.prototype.constructor = Boxer;

window.onload = function(){
	var nameField = document.getElementById('name');
	var surnameField = document.getElementById('surname');
	var patronymicField = document.getElementById('patronymic');
	var weight_categoryField = document.getElementById('weight_category');
	var submitButton = document.getElementById('submit');
	var isNameValid = false, isSurnameValid = false, isPatronymicValid = false, isWeight_categoryValid = false;
	var boxerList = document.getElementById('boxers_list');
	var list = [];

	submitControl();
	function submitControl(){
		if(isNameValid && isSurnameValid && isPatronymicValid && isWeight_categoryValid)
			submitButton.disabled = false;
		else
			submitButton.disabled = true;
	}

	nameField.oninput = function() {
		if(nameField.value === '') {
			isNameValid = false;
			nameField.className = 'error';
		}
		else {
			isNameValid = true;
			nameField.className = 'success';
		}
		submitControl();
	}

	surnameField.oninput = function() {
		if(surnameField.value === '') {
			isSurnameValid = false;
			surnameField.className = 'error';
		}
		else {
			isSurnameValid = true;
			surnameField.className = 'success';
		}
		submitControl();
	}

	patronymicField.oninput = function() {
		if(patronymicField.value === '') {
			isPatronymicValid = false;
			patronymicField.className = 'error';
		}
		else {
			isPatronymicValid = true;
			patronymicField.className = 'success';
		}
		submitControl();
	}

	weight_categoryField.oninput = function() {
		if(weight_categoryField.value === '') {
			isWeight_categoryValid = false;
			weight_categoryField.className = 'error';
		}
		else {
			isWeight_categoryValid = true;
			weight_categoryField.className = 'success';
		}
		submitControl();
	}


	submitButton.onclick = function(event) {
		event.preventDefault();	
		list.push(new Boxer(nameField.value, surnameField.value, patronymicField.value, weight_categoryField.value));
		var elem = document.createElement('li');
		elem.innerHTML = list[list.length - 1].name + " " + list[list.length - 1].surname + " " + list[list.length - 1].patronymic + " " + list[list.length - 1].weight_category;
		boxerList.appendChild(elem);

		var sel = document.getElementById("weight_category");
		var txt = sel.options[sel.selectedIndex].text;
		if (txt === "Heavyweight") {
			elem.className = "color";
		}

		 var box = new Boxer(nameField.value, surnameField.value, patronymicField.value, weight_categoryField.value);
		 console.log(box);

	}
}